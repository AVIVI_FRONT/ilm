# Front-End PUG сборка

Что внутри
<ul>
  <li>Bootstrap 4 grid</li>
  <li>Gulp</li>
  <li>Минификатор картинок</li>
  <li>Спрайт генератор</li>
  <li>SVG Спрайт генератор</li>
  <li>библиотека миксинов bourbon.io</li>
</ul>

# Установка
Перед началалом роботы - установим все пакеты и зависимости

```bash
npm i
```

также поставим gulp =) :
```bash
npm i gulp -g
```

# Начало работы


Для запуска сборки:
```bash
gulp build
```

Для экспорта сборки в продакшн (без livereload, css и js не минифицированы и разбиты по библиотекам):
```bash
gulp production
```

# Для запуска спрайт-генератор:

сначала ложим наши спрайты(иконки etc.) в папку:

```bash
./dist/img/pngspite/
```

после чего пишем:
```bash
gulp sprite
```
спрайты доступны как миксины в - sprite.scss

после чего для подключения спрайта, в scss пишем, @include sprite($'название спрайта') -- например @include sprite($facebook)

# SVG спрайт генератор:

сначала ложим наши спрайты(иконки etc.) в папку:

```bash
./dist/img/svg/
```

после чего пишем:
```bash
gulp svg
```

и готово, для того что б вставить svg в html, достаточно написать команду для вызова pug миксина(в папке templates проверяем что б были подключены миксины)

```bash
+icon(*название иконки с папки*)
```



# Проект запускается на локальном сервере:

```bash
localhost:1337
```

# Пакеты которые есть в проекте

```json
{
  "name": "gl",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "description": "",
  "devDependencies": {
    "bourbon": "^5.0.0-beta.7",
    "gulp": "^3.9.1",
    "gulp-autoprefixer": "^3.1.0",
    "gulp-clean": "^0.3.2",
    "gulp-concat": "^2.6.0",
    "gulp-connect": "^4.1.0",
    "gulp-html-replace": "^1.6.2",
    "gulp-inject": "^4.2.0",
    "gulp-newer": "^1.2.0",
    "gulp-plumber": "^1.1.0",
    "gulp-postcss": "^6.1.1",
    "gulp-pug": "^3.1.0",
    "gulp-replace": "^0.5.4",
    "gulp-resources": "^0.5.0",
    "gulp-sass": "^2.3.2",
    "gulp-sourcemaps": "^1.6.0",
    "gulp-svg-sprite": "^1.3.6",
    "gulp-uglify": "^1.5.3",
    "gulp-watch": "^4.3.8",
    "gulp.spritesmith": "^6.2.1",
    "sassdoc": "^2.1.20"
  }
}


```
