var myMap;

// Дождёмся загрузки API и готовности DOM.
ymaps.ready(init);

var marker_objects = [];

function init () {
    var myMap = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 10,
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        });
    marker_objects[0] = new ymaps.Placemark([55.79549906895957,37.59331800000002], {
        name: '1',
        balloonContent: 'Москва, ул. Сущевский Вал, 5 стр. 1, ТК Савеловский, Детский Центр, павильон L52'
    }, {
        preset: 'islands#dotIcon',
        iconColor: '#4a7de2'
    });
    marker_objects[1] = new ymaps.Placemark([55.66357031515829,37.75293049999994], {
        name: '2',
        balloonContent: 'Москва, ТРК Бум, ул. Перерва, 43к, 1-й этаж'
    }, {
        preset: 'islands#dotIcon',
        iconColor: '#4a7de2'
    });
    marker_objects[2] = new ymaps.Placemark([55.863166568865026,37.6021395], {
        name: '3',
        balloonContent: 'Москва, ул. Декабристов, 12, ТРК Золотой Вавилон, 2-й этаж'
    }, {
        preset: 'islands#dotIcon',
        iconColor: '#4a7de2'
    });
    marker_objects[3] = new ymaps.Placemark([55.65898706908538,37.6187225], {
        name: '4',
        balloonContent: 'Москва, Варшавское шоссе, 68 корпус 2'
    }, {
        preset: 'islands#dotIcon',
        iconColor: '#4a7de2'
    });

    for (var i = 0; i < marker_objects.length; i++) {
        myMap.geoObjects.add(marker_objects[i]);
    }

    $('.find-marker').click(function (e) {
        e.preventDefault();
        var data_marker = $(this).attr('data-marker');
        for (var i = 0; i < marker_objects.length; i++) {
            var obj = marker_objects[i];
            if (obj.properties._data.name === data_marker) {
                myMap.setCenter(obj.geometry._coordinates, 20, {
                    checkZoomRange: true
                });
            }
        }
    });


}