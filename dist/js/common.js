'use strict';
if(!window.console) window.console = {};
if(!window.console.memory) window.console.memory = function() {};
if(!window.console.debug) window.console.debug = function() {};
if(!window.console.error) window.console.error = function() {};
if(!window.console.info) window.console.info = function() {};
if(!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if(!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function(){
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

// placeholder
//-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

    var WW = $(window).width();
    var WH = $(window).height();

    function PosMobileMenu() {
        var top = $('.header__hint').outerHeight(),
            mobMenu = $('.menu-mobile');
        if($('.header__hint').css('display') == 'none') {
            mobMenu.css('height',WH);
            mobMenu.css('top',0);
        } else {
            mobMenu.css('height',WH - top);
            mobMenu.css('top',top);
        }
    }

    function headerHitn() {
        var btn = $('.js-close-header-hint'),
            hintBlock = $('.header__hint'),
            mobMenu = $('.menu-mobile');
        btn.on('click',function () {
            hintBlock.slideUp(200);
            mobMenu.css('top',0);
            mobMenu.css('height',WH);
        });
    }

    function topSlider(){
        var slider = $('#top-slider'),
            sliderText = $('#text-slider');
        slider.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: sliderText,
            autoplay: true,
            autoplaySpeed: 4000
        });
        sliderText.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            speed: 200,
            fade: true,
            cssEase: 'linear',
            asNavFor: slider
        });
    }

    function collectionSliderText(){
        var sliderText = $('#text-collection-slider');
        sliderText.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            speed: 200,
            fade: true,
            cssEase: 'linear',
            swipe: false,
            accessibility: false
        });
    }

    function actualSlider(){
        var slider = $('#actual-slider'),
            sliderSecond = $('#actual-slider-mobile');
        if ($(window).width() > 543) {
            $('.catalog__box-item.mobile').remove();
        }
        slider.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3500,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 543,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        asNavFor: sliderSecond
                    }
                },
                {
                    breakpoint: 440,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        asNavFor: sliderSecond
                    }
                }
            ]
        });
        sliderSecond.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            autoplay: false,
            responsive: [
                {
                    breakpoint: 543,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        dots: true,
                        asNavFor: slider
                    }
                },
                {
                    breakpoint: 440,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true,
                        asNavFor: slider
                    }
                }
            ]
        });
        $('.js-actual-arrows').on('click', function(e) {
            e.preventDefault();
            slider.slick($(this).attr('data-slider'));
        });
    }

    function validateMainSubs(){
        var btn = $('.js-main-subs-btn');
        btn.on('click',function (e) {
            e.preventDefault();
            $('#js-main-subs').submit();
        });

        $('#js-main-subs').validate({
            rules: {
                mail: {
                    required: true
                }
            },
            submitHandler: function() {
                alert("Form Submitted! Email: " + $('.main__news-subs-input').val());
            }
        });
    }

    function openSection(){
        $('.return__cross').on('click', function (e) {
            e.preventDefault();
            // $(this).parents('.return__box').toggleClass('show');
        });
    }

    function mobMenu(){
        var btn = $('.js-mobile-menu'),
            closeBtn = $('.js-close-menu'),
            mobMenu = $('.menu-mobile'),
            overlay = $('.overlay');
        btn.on('click',function (e) {
            e.preventDefault();
            overlay.fadeIn();
            mobMenu.addClass('active');
        });
        closeBtn.on('click',function (e) {
            e.preventDefault();
            mobMenu.removeClass('active');
            overlay.fadeOut();
        });
        overlay.on('click',function () {
            mobMenu.removeClass('active');
            overlay.fadeOut();
        });
    }

    function mobCatalog() {
        var btn = $('.js-catalog-mob'),
            btnSubCatalog = $('.js-mobile-submenu');
        btn.on('click', function () {
            $(this).parents('.menu-mobile__name').toggleClass('active');
        });
        btnSubCatalog.on('click', function (e) {
            e.preventDefault();
            $(this).parents('.menu__submenu-block').toggleClass('active');
        });
    }

    function openTabs(){
        $('.tab').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('.tab').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
        })
    }

    function mobCollection(){
        var sliderText = $('.js-mobile-col-slider-title'),
            slider = $('.js-mobile-col-slider');
        sliderText.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3500,
            asNavFor: slider,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
        slider.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            asNavFor: sliderText,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
    }

    function newsMobileSlider(){
        var slider = $('#news-slider-mobile');
        slider.slick({
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
        });
    }

    function instaSlider(){
        var slider = $('#js-insta-slider');
        if ($(window).width() < 769) {
            if (!slider.hasClass('slick-initialized')) {
                slider.slick({
                    infinite: true,
                    arrows: false,
                    dots: true,
                    // autoplay: true,
                    // autoplaySpeed: 3000,
                    slidesPerRow: 3,
                    rows: 2,
                    responsive: [
                        {
                            breakpoint: 543,
                            settings: {
                                slidesPerRow: 2,
                                rows: 2,
                                dots: true,
                            }
                        }
                    ]
                });
            }
            $('.main__insta-img.mobile').css('height',$('.main__insta-img.mobile').outerWidth())
        } else {
            if (slider.hasClass('slick-initialized')) {
                slider.slick('unslick');
            }
        }
    }

    function openGoods(){
        $('.card__more').on('click', function (e) {
            e.preventDefault();
            $(this).parents('.card__colors').toggleClass('opened');
        });
    }

    function openStore(){
        $('.card__store-cross').on('click', function (e) {
            e.preventDefault();
            $(this).parents('.card__store-box').toggleClass('opened');
        });
    }

    function openMobTabs(){
        $('.card__cross').on('click', function () {
            $(this).parents('.card__tab').toggleClass('active');
        });
    }

    function openFixedSize(){
        $('.btn__size').on('click', function () {
            $(this).parents('.card__fixed-wrap').addClass('size-opened');
            $('.page-wrapper').addClass('shadow')
        });
    }

    function closeFixedSize(){
        $('.card__size-cross').on('click', function () {
            $(this).parents('.card__fixed-wrap').toggleClass('size-opened');
            $('.page-wrapper').removeClass('shadow')
        });
        $('.overlay').on('click', function () {
            $('.card__fixed-wrap').removeClass('size-opened');
            $('.overlay').fadeOut();
        })
    }

    function openCart() {
        $('.header__ico-bag').on('click', function (e) {
            e.preventDefault();
            $('.cart').addClass('opened');
            $('.overlay').fadeIn();
        });

        $('.overlay').on('click', function () {
            $('.card__fixed-wrap').removeClass('size-opened');
            $('.overlay').fadeOut();
        })
    }

    function closeCart() {
        $('.cart__cross').on('click', function () {
            $('.cart').removeClass('opened');
            $('.overlay').fadeOut();
        });
        $('.overlay').on('click', function () {
            $('.cart').removeClass('opened');
            $('.overlay').fadeOut();
        })
    }

    function formStyler(){
        $('select').styler();
    }

    function mask(){
        $('.mask').mask('(000) 000-0000');
    }

    function textareaSize(){
        autosize($('textarea'));
    }

    function checkoutForm(){
        var form = $('#checkout-form'),
            btn = $('.js-checkout-submit');
        btn.on('click',function () {
            form.submit();
        })
    }

    function catalogClose() {
        var btn = $('.js-catalog-close'),
            info = $('.catalog__box-info');
        btn.on('click', function (e) {
            e.preventDefault();
            $(this).parents('.catalog__box-item').find(info).removeClass('hover active');
        });
    }

    function catalogHover() {
        $('.catalog__box-item').hover(
            function() {
                // $(this).find('.catalog__box-info').addClass('hover');
                $(this).find('.js-catalog-zoom').addClass('active');
            }, function() {
                // $(this).find('.catalog__box-info').removeClass('hover');
                $(this).find('.js-catalog-zoom').removeClass('active');
            }
        );
    }

    function accordion() {
        $('.return__cross').click(function(e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.prev().hasClass('show')) {
                $this.prev().removeClass('show');
                $('.return__box').removeClass('show');
                $this.prev().slideUp(350);
            } else {
                $this.parent().parent().find('.return__desc').removeClass('show');
                $this.parent().parent().find('.return__desc').slideUp(350);
                $this.prev().toggleClass('show');
                $('.return__box').removeClass('show');
                $this.parents('.return__box').addClass('show');
                $this.prev().slideToggle(350);
            }
        });
    }

    function buy(){
        var btn = $('.js-catalog-cart');
        btn.on('click',function (e) {
            e.preventDefault();
            $(this).parents('.catalog__box-item').find('.catalog__box-info').addClass('active')
        });
    }

    function filter() {
        var btn = $('.js-catalog-filter'),
            filter = $('#filter'),
            close = $('.js-close-filter'),
            overlay = $('.overlay'),
            filterBox = $('.js-filter .filter__content-title'),
            filterItem = $('.js-filter .filter__link'),
            reset = $('.filter__btn-reset');
        btn.on('click',function (e) {
            e.preventDefault();
            filter.addClass('active');
            overlay.fadeIn(300);
        });
        close.on('click',function (e) {
            e.preventDefault();
            filter.removeClass('active');
            overlay.fadeOut(300);
        });
        overlay.on('click',function (e) {
            e.preventDefault();
            filter.removeClass('active');
            overlay.fadeOut(300);
        });
        filterBox.on('click',function (e) {
            e.preventDefault();
            $(this).parents('.filter__item').toggleClass('active');
        });
        filterItem.on('click',function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
        });
        reset.on('click',function (e) {
            e.preventDefault();
            filterItem.removeClass('active');
        });
    }
    
    function slider_1(){
        function detect_active(){
            var get_active = $("#dp-slider .dp_item:first-child").data("class");
            $("#dp-slider #dp-dots li").removeClass("active");
            $("#dp-slider #dp-dots li[data-class="+ get_active +"]").addClass("active");
        }

        $("body").on("click", "#dp-slider .dp_item:not(:first-child)", function(){
            var get_slide = $(this).attr('data-class');
            $("#dp-slider .dp_item[data-class=" + get_slide + "]").hide().prependTo("#dp-slider").fadeIn();
            $.each($('#dp-slider .dp_item'), function (index, dp_item) {
                $(dp_item).attr('data-position', index + 1);
            });
            $('#text-collection-slider').slick('slickGoTo', get_slide);

            detect_active();
        });
    }
    function slider_2(){
        function detect_active(){
            var get_active = $("#dp-slider2 .dp_item:first-child").data("class");
            $("#dp-slider2 #dp-dots li").removeClass("active");
            $("#dp-slider2 #dp-dots li[data-class="+ get_active +"]").addClass("active");
        }

        $("body").on("click", "#dp-slider2 .dp_item:not(:first-child)", function(){
            var get_slide = $(this).attr('data-class');
            $("#dp-slider2 .dp_item[data-class=" + get_slide + "]").hide().prependTo("#dp-slider2").fadeIn();
            $.each($('#dp-slider2 .dp_item'), function (index, dp_item) {
                $(dp_item).attr('data-position', index + 1);
            });

            detect_active();
        });
    }

    function mobSort (){
        if ($('html').hasClass('mobile')) {
            var select = $('.catalog__sort select');
            select.show();
            select.css('z-index','100');
            select.css('width','calc(100% + 20px)');
            $('.jq-selectbox').addClass('opened');
            $('.jq-selectbox__dropdown').hide();
        }
    }


    topSlider();
    collectionSliderText();
    actualSlider();
    formStyler();
    mobSort ();

    $(window).on('resize',function () {
        PosMobileMenu();
        instaSlider();
    });

    function preloader (){
        $('.preloader').removeClass('preloader');
    }

    $(document).ready(function () {
        headerHitn();
        validateMainSubs();
        openSection();
        mobMenu();
        mobCatalog();
        PosMobileMenu();
        openTabs();
        mobCollection();
        newsMobileSlider();
        instaSlider();
        openGoods();
        openStore();
        mask();
        textareaSize();
        openMobTabs();
        openFixedSize();
        closeFixedSize();
        checkoutForm();
        catalogClose();
        catalogHover();
        openCart();
        closeCart();
        accordion();
        buy();
        filter();
        slider_1();
        slider_2();
        preloader();
    });

    if ($(".card__info .card__buttons").length > 0) {
        $(window).scroll(function () {
            var offsetFromScreenTop = $(".card__info .card__buttons").offset().top - $(window).scrollTop() + $(".card__info .card__buttons").height();
            if (offsetFromScreenTop < 0) {
                $('.card__fixed-wrap').addClass('visibility');
            }
            else {
                $('.card__fixed-wrap').removeClass('visibility');
            }
        })
    }
});
